/* прототипное наследование это когда от класа родителя его потомки могут
наследовать методы и свойства и использовать их в себе и в своих потомках
для того что б не дублировать код и не менять информацию, также наследовать
методы и свойства могут множества потомков. */

class Employee {
  constructor(name, age, salary){
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name(){
    return this._name;
  }
  get age(){
    return this._age;
  }
  get salary(){
    return this._salary;
  }
}

class Programmer extends Employee {
  get salary (){
    return super.salary * 3;
  }
}
const employee = new Employee('richard',32,3000);
const programer1 = new Programmer('victoria',22,3000);
const programer2 = new Programmer('italii',38,3000);
const programer3 = new Programmer('ban',27,3000);


console.log(employee.name, employee.age, employee.salary);
console.log(programer1.name, programer1.age, programer1.salary);
console.log(programer2.name, programer2.age, programer2.salary);
console.log(programer3.name, programer3.age, programer3.salary);

