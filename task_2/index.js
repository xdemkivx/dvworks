const beacon = [
    'https://baconmockup.com/600/300/',
    'https://baconmockup.com/600/301/',
    'https://baconmockup.com/600/302/',
    'https://baconmockup.com/600/303/',
    'https://baconmockup.com/600/299/',
];

const bear = [
    'https://placebear.com/600/303',
    'https://placebear.com/600/302',
    'https://placebear.com/600/301',
    'https://placebear.com/600/300',
    'https://placebear.com/600/299',
    'https://placebear.com/600/298',
    'https://placebear.com/600/297',
    'https://placebear.com/600/296',
    'https://placebear.com/600/295',
];

const dog = [
    'https://www.purina.ua/dog/dog-chow/sites/default/files/2019-07/article-29_830x554.jpg',
]

const cat = [
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTkU9Yu1E6xGNdcVJF5DcMX9a5mKFEU-vPciw&usqp=CAU',
]


const btnPrev = document.querySelector('.btn-prev')
const btnNext = document.querySelector('.btn-next')

const categories = {
    beacon,
    bear,
    dog,
    cat,
};

let currentImg = 0;
let currentCategory = 'beacon';


const imgContainer = document.querySelector('.image-holder')

const changeBackgroundImage = () => {
    imgContainer.style.backgroundImage = `url("${categories[currentCategory][currentImg]}")`;
}

btnPrev.addEventListener('click', () => {
    if (currentImg > 0) {
        currentImg -= 1
        changeBackgroundImage();
    }
});

btnNext.addEventListener('click', () => {
    if (currentImg < categories[currentCategory].length - 1) {
        currentImg += 1
        changeBackgroundImage();
    }
});

const btnCollection = document.querySelectorAll('.category-container button');

btnCollection.forEach(btn => {
    btn.addEventListener('click', (event)=> {
        currentCategory = event.currentTarget.dataset.category;
        currentImg = 0;
        changeBackgroundImage();
    });
});

// document.querySelectorAll('.bullet-container span').forEach(span => {
//     span.addEventListener('click', (event) => {
//         console.log(event.target.dataset.index);
//         document.querySelector('.bullet-active').classList.remove('bullet-active');
//         event.target.classList.add('bullet-active');
//     })
// })