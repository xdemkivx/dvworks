const graphicd = {
  "graphic-design1" : {
    "name" : 'Graphic Design',
  },
  "graphic-design2" : {
    "name" : 'Graphic Design',
  },
  "graphic-design3" : {
    "name" : 'Graphic Design',
  },
  "graphic-design4" : {
    "name" : 'Graphic Design',
  },
  "graphic-design5" : {
    "name" : 'Graphic Design',
  },
  "graphic-design6" : {
    "name" : 'Graphic Design',
  },
  "graphic-design7" : {
    "name" : 'Graphic Design',
  },
  "graphic-design8" : {
    "name" : 'Graphic Design',
  },
  "graphic-design9" : {
    "name" : 'Graphic Design',
  },
  "graphic-design10" : {
    "name" : 'Graphic Design',
  },
  "graphic-design11" : {
    "name" : 'Graphic Design',
  },
  "graphic-design12" : {
    "name" : 'Graphic Design',
  },
  
}
const landing = {
  "landing-page1" : {
    "name" : 'Landing Pages',
  },
  "landing-page2" : {
    "name" : 'Landing Pages',
  },
  "landing-page3" : {
    "name" : 'Landing Pages',
  },
  "landing-page4" : {
    "name" : 'Landing Pages',
  },
  "landing-page5" : {
    "name" : 'Landing Pages',
  },
  "landing-page6" : {
    "name" : 'Landing Pages',
  },
  "landing-page7" : {
    "name" : 'Landing Pages',
  },
}
const web = {
  "web-design1" : {
    "name" : 'Web Design',
  },
  "web-design2" : {
    "name" : 'Web Design',
  },
  "web-design3" : {
    "name" : 'Web Design',
  },
  "web-design4" : {
    "name" : 'Web Design',
  },
  "web-design5" : {
    "name" : 'Web Design',
  },
  "web-design6" : {
    "name" : 'Web Design',
  },
  "web-design7" : {
    "name" : 'Web Design',
  },
}
const wordpres = {
  "wordpress1" : {
    "name" : 'Wordpress',
  },
  "wordpress2" : {
    "name" : 'Wordpress',
  },
  "wordpress3" : {
    "name" : 'Wordpress',
  },
  "wordpress4" : {
    "name" : 'Wordpress',
  },
  "wordpress5" : {
    "name" : 'Wordpress',
  },
  "wordpress6" : {
    "name" : 'Wordpress',
  },
  "wordpress7" : {
    "name" : 'Wordpress',
  },
  "wordpress8" : {
    "name" : 'Wordpress',
  },
  "wordpress9" : {
    "name" : 'Wordpress',
  },
  "wordpress10" : {
    "name" : 'Wordpress',
  },
}