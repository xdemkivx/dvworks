/* const swiper = new Swiper('.swiper', { */
  // Optional parameters
  /* direction: 'vertical',
  loop: true, */

  // If we need pagination
  /* pagination: {
    el: '.swiper-pagination',
    clickable: true,
    dynamicBullets: true,
  },
 */
  // Navigation arrows
 /*  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  }, */

  // And if we need scrollbar
  /* scrollbar: {
    el: '.swiper-scrollbar',
  }, */
/* }); */
let swiper = new Swiper(".mySwiper", {
  spaceBetween: 10,
  slidesPerView: 4,
  freeMode: true,
  watchSlidesProgress: true,
  
});
let swiper2 = new Swiper(".mySwiper2", {
  spaceBetween: 10,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  thumbs: {
    swiper: swiper,
  },
  scrollbar: {
    el: '.swiper-scrollbar',
  },
});