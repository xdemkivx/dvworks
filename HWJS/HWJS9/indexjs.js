/* ## Теоретические вопросы
1. Опишите каким образом можно создать новый HTML на странице.
document.createElement(tag) с помощью даного метода можно создать елемент с нужным тегом,
document.createTextNode(text) можно создать тестовый елемент на странице.


2. Опишите что означает первый параметр функции insertAdjacentHTML и опишите возможные варианты 
этого параметра. 
Этим елементом непосредственно через ДЖС можно вставить хтмл код любому елемнте в дом дерево, 
с помощью вспомогательных атрибутов, хтмл можна вставить непосредственно перед елементом, 
сразу после елемента, в дочерних елемнтах, или после вего блока елемента.


3. Как можно удалить элемент со страницы?
лучше всего методом remove.

*/

const list = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const listHtml = list.map((elem) => `<li>${elem}</li>`);

const ulElem = document.querySelector("#ulconteiner");
ulElem.insertAdjacentHTML("afterbegin", listHtml.join(""));
console.log(listHtml);