/* ## Теоретические вопросы
1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
Это полностью описаный хтм в легах, к которому с помощью определенных команд 
можно обращатся, взаимодействовать с ним в живом времени, который прогружаеться сверху в низ.
2. Какая разница между свойствами HTML-элементов innerHTML и innerText?
innerHTML - выводит текст вместе с ХТМ тегами такими как имг, р, спан, и прочее.
innerText - выводит только сам текст который находится в хтм странице.

3. Как можно обратится к элементу страницы с помощью JS? Какой способ предпочтительнее? 
предпочительные свойства document.querySelector и document.querySelectorAll.
также есть и другые варианты с etElementById() ну это боле старые способы.

*/



const paragraf = document.querySelectorAll("p");
paragraf.forEach(elem => elem.classList.add('all-paragraf-fone'))


const idOptionList = document.querySelector("#optionsList");
console.log(idOptionList);
console.log(idOptionList.parentNode);
console.log(idOptionList.childNodes);
console.log(idOptionList.children);


const testParagraph = document.querySelectorAll(".testParagraph");
testParagraph.textContent = `<p>This is a paragraph</p>`;
console.log(testParagraph.textContent);
console.log(testParagraph);


const mainHeade= document.querySelector(".main-header");

const li = mainHeade.querySelectorAll("li");
li.forEach(elem => elem.classList.add ('nav-item'))

console.log(mainHeade);

console.log(li);

const sectionTitle = document.querySelectorAll('section-title');
sectionTitle.forEach(elev => elem.classList.remove ('section-title'))
console.log(sectionTitle.classList);
// section-title такого класа вообще не было, показал как бы его удалил если б он был)