/*
тру кетч используют тогда когда код должен всегда работать а не падать, 
при этом отлавливать ошибки которые в будущем надо исправлять,
тлт для фильтрации вывода нужной информации.  
 */
const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
     
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];


class NotItemError extends Error {
    constructor(author,name,price) {
        super();
        this.author = "NotItemError";
        this.name= "NotItemError";
        this.price = "NotItemError";
        this.message = `Invalid format of ITEM BOOKS: ${author}`;
        this.message = `Invalid format of ITEM BOOKS: ${name}`;
        this.message = `Invalid format of ITEM BOOKS: ${price }`;
    }
}

class BooksItem {
    constructor({author,name,price}) {
        if (author === undefined || name === undefined || price === undefined) {
            throw new NotItemError(author, name, price);
        }
        
        this.author = author;
        this.name = name;
        this.price = price;
       // console.log(this.author);
        //console.log(this.name);
        //console.log(this.price);

    }

    render(container) {
        container.insertAdjacentHTML('beforeend', `
        <div id="root" >
            <ul>
            <li>${this.author}</li>
            <li>${this.name}</li>
            <li>${this.price}</li>
            </ul>
        </div>`)
        /* console.log(this.author);
        console.log(this.name);
        console.log(this.price); */
    }
}
const container = document.querySelector(".books");

books.forEach((el) => {
    try {
        new BooksItem(el).render(container);
    } catch (err) {
        if (err.author === "NotItemError" || err.name === "NotItemError" || err.price === "NotItemError") {
            console.warn(err);
        } else {
            throw err;
        }
    }
})

